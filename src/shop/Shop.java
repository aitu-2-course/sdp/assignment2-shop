package shop;

import products.Product;

import java.util.ArrayList;

public class Shop {

    private Basket basket = new Basket();
    private ArrayList<Product> products = new ArrayList<>();
    private int ID = 0;
    private static Shop instance = null;

    private Shop(){}
    public static Shop getInstance(){
        if(instance==null) instance = new Shop();
        return instance;
    }

    public void setUser(IUser user){
        this.user = user;
    }

    public ArrayList<Product> getProducts(){
        return products;
    }

    public void displayProduct(){
        for(Product product: products){
            System.out.println(product.getFormat());
            System.out.println(product.getAdditionalInfo());
            System.out.println("_______________________________________________");
        }
    }

    private int nextProductId(){
        return ++ID;
    }

    public void addProduct(Product product){
        product.setId(nextProductId());
        products.add(product);
    }

    public void deleteProductById(int id){
        for(Product product: products){
            if(product.getId()==id){
                products.remove(product);
                break;
            }
        }
    }

    public void addToBasket(int id, int quantity){
        for(Product product: products){
            if(product.getId()==id){
                basket.addProduct(product, quantity);
                break;
            }
        }
    }


    public void makeTransaction(){
        Transaction.makeTransaction(basket);
    }



}
