package shop;

import products.Product;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Transaction {
    public static void makeTransaction(Basket basket){
        double overAllPrice = 0.0;
        double cgst, sgst, subtotal=0.0, discount=0.0;
        File file = new File("transactions.txt");
        try {
            String text = "";
            FileWriter writer = new FileWriter(file, true);
            text += "\n\t\t\t\t--------------------Invoice-----------------";
            text += "\n\t\t\t\t\t "+"  "+"Metro Mart Grocery Shop";
            text += "\n\t\t\t\t\t3/98 Mecrobertganj New Mumbai";
            text += "\n\t\t\t\t\t"  +"    " +"Opposite Metro Walk";
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            String[] days = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            text += "\nDate: "+formatter.format(date)+"  "+days[calendar.get(Calendar.DAY_OF_WEEK) - 1]+"\t\t\t\t\t\t (+91) 9998887770";
            text += "\nProduct ID \t\tName\t\tQuantity\t\tRate \t\t\t\tTotal Pice\n";

            for(Product product: basket.getBasket()){
                overAllPrice += product.getPrice()*product.getQuantity();
                String line = String.format("   %-9s    %-9s      %5d     %9.2f        %14.2f\n",
                        product.getId(), product.getName(), product.getQuantity(), product.getPrice(),
                        product.getQuantity()*product.getPrice());
                text += line;
            }

            text += "\n\t\t\t\t\t\t\t\t\t\tTotal Amount (Rs.) " + overAllPrice;
            discount = overAllPrice*2/100;
            text += "\n\t\t\t\t\t\t\t\t\t\t    Discount (Rs.) " + discount;
            subtotal = overAllPrice - discount;
            text += "\n\t\t\t\t\t\t\t\t\t\t          Subtotal " + subtotal;
            sgst=overAllPrice*12/100;
            text += "\n\t\t\t\t\t\t\t\t\t\t          SGST (%) "+sgst;
            cgst=overAllPrice*12/100;
            text += "\n\t\t\t\t\t\t\t\t\t\t          CGST (%) "+cgst;
            text += "\n\t\t\t\t\t\t\t\t\t\t     Invoice Total " +(subtotal+cgst+sgst);
            text += "\n\t\t\t\t----------------Thank You for Shopping!!-----------------";
            text += "\n\t\t\t\t                     Visit Again";
            writer.close();
            System.out.println(text);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
