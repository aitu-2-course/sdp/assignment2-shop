package shop;

import products.Product;
import products.interfaces.IProduct;

import java.util.ArrayList;

public class Basket {

    private ArrayList<Product> basket = new ArrayList<>();

    public void addProduct(Product product, int quantity){
        product.setQuantity(quantity);
        basket.add(product);
    }

    public void deleteProduct(Product product){
        for(Product prod: basket){
            if(prod.getId()==product.getId()){
                basket.remove(prod);
            }
        }
    }

    public void emptyBasket(){
        basket.clear();
    }

    public ArrayList<Product> getBasket(){
        return basket;
    }
}
