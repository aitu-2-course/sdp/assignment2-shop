package menus;

import products.Grocery;
import shop.Shop;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class AddGroceryMenu extends Menu{
    Shop shop = Shop.getInstance();
    public AddGroceryMenu(String name) {
        super(name);
    }

    @Override
    public void run(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String name = getString("Enter grocery name: ");
        int quantity = getInt("Enter quantity: ");
        double price = getDouble("Enter price: ");
        input.nextLine();
        Date manufacturingDate = null;
        Date expirationDate = null;
        try {
            manufacturingDate = sdf.parse(getString("Enter manufacturing date (dd-MM-yyyy): "));
            expirationDate = sdf.parse(getString("Enter expiration date (dd-MM-yyyy): "));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        shop.addProduct(new Grocery(0, name, quantity, price, manufacturingDate, expirationDate));
    }
}
