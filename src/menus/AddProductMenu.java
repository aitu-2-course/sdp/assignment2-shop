package menus;

import products.factory.FactoryProducer;
import products.factory.ProductFactory;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Scanner;

public class AddProductMenu extends Menu {


    public AddProductMenu(String name) {
        super(name);
    }

    @Override
    public void run() {

        addOption(1, "Grocery");
        addOption(2, "Appliance");
        printOptions();
        int choice = getInt("Enter choice: ");
        ProductFactory productFactory = null;
        if(choice==1) productFactory = FactoryProducer.getFactory("Grocery");
        else if(choice==2) productFactory = FactoryProducer.getFactory("Appliance");
        productFactory.getMenu().run();


    }
}
