package menus;

import shop.Shop;

import java.text.ParseException;

public class DeleteProductMenu extends Menu{
    Shop shop = Shop.getInstance();
    public DeleteProductMenu(String name) {
        super(name);
    }

    @Override
    public void run(){
        int prodId = getInt("Enter id of product to delete: ");
        shop.deleteProductById(prodId);
    }
}
