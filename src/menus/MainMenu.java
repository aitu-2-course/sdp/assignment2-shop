package menus;

import java.util.HashMap;

public class MainMenu extends Menu{


    public MainMenu(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Welcome to " + name +" menu!");
        addOption(1, "Buy products");
        addOption(2, "Admin panel");
        addOption(0, "Quit");

        int choice;
        do{
            printOptions();
            choice = getInt("Enter choice: ");
            switch (choice){
                case 1:
                    new ClientMenu("Buy products").run();
                    break;
                case 2:
                    new AdminMenu("Admin panel").run();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Unknown option!");
                    break;
            }
        }while (choice!=0);

    }
}
