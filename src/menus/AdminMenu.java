package menus;

public class AdminMenu extends Menu{
    public AdminMenu(String name) {
        super(name);
    }

    @Override
    public void run() {
        addOption(1, "Add new product");
        addOption(2, "Delete product");
        addOption(0, "Quit");

        int choice;
        do{
            printOptions();
            choice = getInt("Enter choice: ");
            switch (choice){
                case 1:
                    new AddProductMenu("Add product").run();
                    break;
                case 2:
                    new DeleteProductMenu("Delete product").run();
                case 0:
                    break;
                default:
                    System.out.println("Unknown option!");
                    break;
            }
        }while (choice!=0);
    }
}
