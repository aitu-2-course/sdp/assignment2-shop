package menus;

import java.text.ParseException;

public interface IMenu {

    void printOptions();

    void run();
}
