package menus;

import shop.Shop;

public class AddToBasketMenu extends Menu{
    Shop shop = Shop.getInstance();
    public AddToBasketMenu(String name) {
        super(name);
    }

    @Override
    public void run() {
        int prodId = getInt("Enter id of product to add to basket: ");
        int quantity = getInt("Enter quantity: ");
        shop.addToBasket(prodId,quantity);
    }
}
