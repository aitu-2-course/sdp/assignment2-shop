package menus;

import products.Appliance;
import shop.Shop;

import java.text.ParseException;
import java.util.Scanner;

public class AddApplianceMenu extends Menu{
    Shop shop = Shop.getInstance();
    public AddApplianceMenu(String name) {
        super(name);
    }

    @Override
    public void run() {
        String name = getString("Enter appliance name: ");
        int quantity = getInt("Enter quantity: ");
        double price = getDouble("Enter price: ");
        double powerConsumption = getDouble("Enter power consumption (W): ");
        input.nextLine();
        String instruction = getString("Enter instruction: ");
        shop.addProduct(new Appliance(0, name, quantity, price, powerConsumption, instruction));
    }
}
