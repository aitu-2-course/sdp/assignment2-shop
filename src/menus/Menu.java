package menus;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public abstract class Menu implements IMenu{
    protected Scanner input = new Scanner(System.in);
    protected HashMap<Integer, String> options = new HashMap<>();
    protected String name;
    protected String endline = "______________________________________________________";



    public Menu(String name){
        this.name = name;

    }

    protected void addOption(int optionNum, String option){
        options.put(optionNum, option);
    }

    protected int getInt(String prompt){
        System.out.println(prompt);
        return input.nextInt();
    }

    protected String getString(String prompt){
        System.out.println(prompt);
        return input.nextLine();
    }

    protected double getDouble(String prompt){
        System.out.print(prompt);

        return input.nextDouble();
    }



    @Override
    public void printOptions(){
        System.out.println(endline);
        System.out.println("Welcome to " + name + " menu");
        for(Integer optNum:options.keySet()){
            System.out.println(optNum+". " + options.get(optNum));
        }

    }
}
