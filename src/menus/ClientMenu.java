package menus;

import shop.Shop;

import java.util.HashMap;

public class ClientMenu extends Menu{
    Shop shop = Shop.getInstance();
    public ClientMenu(String name) {
        super(name);
    }

    @Override
    public void run() {
        addOption(1,"View products");
        addOption(2, "Add to basket");
        addOption(3, "Buy");
        addOption(0, "Quit");
        int choice;
        do{
            printOptions();
            choice = getInt("Enter choice: ");
            switch (choice){
                case 1:
                    shop.displayProduct();
                    break;
                case 2:
                    new AddToBasketMenu("Add to basket").run();
                    break;
                case 3:
                    shop.makeTransaction();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Unknown option!");
                    break;

            }
        }while (choice!=0);
    }
}
