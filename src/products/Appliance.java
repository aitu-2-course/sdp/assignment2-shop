package products;

public class Appliance extends Product{
    private double powerConsumptionW;
    private String instruction;

    public Appliance(){}

    public Appliance(int id, String name, int quantity, double price, double powerConsumptionW, String instruction) {
        super(id, name, quantity, price);
        this.powerConsumptionW = powerConsumptionW;
        this.instruction = instruction;
    }


    @Override
    public String getAdditionalInfo() {
        return "Power consumption in Watts: " + powerConsumptionW + "\nInstruction: " + instruction;
    }
}
