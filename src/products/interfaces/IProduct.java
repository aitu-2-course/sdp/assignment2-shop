package products.interfaces;

public interface IProduct {
    String getFormat();
    String getAdditionalInfo();

}
