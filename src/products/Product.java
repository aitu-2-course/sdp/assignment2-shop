package products;

import products.factory.ProductFactory;
import products.interfaces.IProduct;

public abstract class Product implements IProduct {
    private int id;
    private String name;
    private int quantity;
    private double price;

    public Product(){}

    public Product(int id, String name, int quantity, double price) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;

    }

    @Override
    public String getFormat(){
        return "ID: " + id+
                "\nProduct name: " + name+
                "\nQuantity: " + quantity+
                "\nPrice: " + price + " tenge";
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }


    public Product setId(int id) {
        this.id = id;
        return this;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public Product setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public Product setPrice(double price) {
        this.price = price;
        return this;
    }

}
