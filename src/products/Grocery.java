package products;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Grocery extends Product{
    private Date manufacturingDate;
    private Date expirationDate;

    public Grocery(){}

    public Grocery(int id, String name, int quantity, double price, Date manufacturingDate, Date expirationDate) {
        super(id, name, quantity, price);
        this.manufacturingDate = manufacturingDate;
        this.expirationDate = expirationDate;
    }



    @Override
    public String getAdditionalInfo() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return "Manufacturing date: " + sdf.format(manufacturingDate) +
                "\nExpiration date: " + sdf.format(expirationDate);
    }
}
