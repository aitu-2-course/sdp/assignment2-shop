package products.factory;

import menus.IMenu;
import products.interfaces.IProduct;

public interface ProductFactory {
    IProduct createProduct();
    IMenu getMenu();
}
