package products.factory;

import menus.AddGroceryMenu;
import menus.IMenu;
import products.Grocery;
import products.interfaces.IProduct;

public class GroceryFactory implements ProductFactory{

    @Override
    public IProduct createProduct() {
        return new Grocery();
    }

    @Override
    public IMenu getMenu() {
        return new AddGroceryMenu("Add grocery");
    }
}
