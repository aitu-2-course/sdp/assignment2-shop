package products.factory;

import menus.AddApplianceMenu;
import menus.AddProductMenu;
import menus.IMenu;
import products.Appliance;
import products.interfaces.IProduct;

public class ApplianceFactory implements ProductFactory{
    @Override
    public IProduct createProduct() {
        return new Appliance();
    }

    @Override
    public IMenu getMenu() {
        return new AddApplianceMenu("Add appliance");
    }
}
