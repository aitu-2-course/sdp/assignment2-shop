package products.factory;

public class FactoryProducer {
    public static ProductFactory getFactory(String factoryName){
        if(factoryName.equals("Appliance")) return new ApplianceFactory();
        else if(factoryName.equals("Grocery")) return new GroceryFactory();
        else return null;
    }
}
