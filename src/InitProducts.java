import products.Grocery;
import products.Product;
import shop.Shop;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class InitProducts {
    private Shop shop = Shop.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    public InitProducts() {
        ArrayList<Product> products = new ArrayList<>();
        try {
            products.add(new Grocery(0, "Bread", 100,
                    180., sdf.parse("06-10-2023"), sdf.parse("07-10-2023")));
            products.add(new Grocery(0, "Milk", 100,
                    400., sdf.parse("06-10-2023"), sdf.parse("07-10-2023")));
            products.add(new Grocery(0, "Beef", 100,
                    1500,  sdf.parse("06-10-2023"), sdf.parse("07-10-2023")));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }


        for(Product product:products){
            shop.addProduct(product);
        }
    }
}
