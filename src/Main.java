import menus.MainMenu;
import shop.Shop;

import java.text.ParseException;

public class Main {
    public static void main(String[] args) {
        new InitProducts();
        MainMenu mainMenu = new MainMenu("Main menu");
        mainMenu.run();
    }
}
